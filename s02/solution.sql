-- Creating the blog_db database
CREATE DATABASE blog_db;
USE blog_db;

-- Creating the 'users' table
CREATE TABLE users (
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(100),
  password VARCHAR(300),
  datetime_created DATETIME,
  PRIMARY KEY(id)
);

-- Creating the 'posts' table
CREATE TABLE posts (
  id INT NOT NULL AUTO_INCREMENT,
  author_id INT,
  title VARCHAR(500),
  content VARCHAR(5000),
  datetime_posted DATETIME,
  PRIMARY KEY(id),
  CONSTRAINT fk_posts_user_id
    FOREIGN KEY (author_id) REFERENCES users (id) 
    ON UPDATE CASCADE 
    ON DELETE RESTRICT
);

-- Creating the 'post_comments' table
CREATE TABLE post_comments (
  id INT NOT NULL AUTO_INCREMENT,
  post_id INT,
  user_id INT,
  content VARCHAR(5000),
  datetime_commented DATETIME,
  PRIMARY KEY(id),
  CONSTRAINT fk_post_comments_post_id
    FOREIGN KEY (post_id) REFERENCES posts (id) 
    ON UPDATE CASCADE 
    ON DELETE RESTRICT,
  CONSTRAINT fk_post_comments_user_id
    FOREIGN KEY (user_id) REFERENCES users (id) 
    ON UPDATE CASCADE 
    ON DELETE RESTRICT
);

-- Creating the 'post_likes' table
CREATE TABLE post_likes (
  id INT NOT NULL AUTO_INCREMENT,
  post_id INT,
  user_id INT,
  datetime_like DATETIME,
  PRIMARY KEY(id),
  CONSTRAINT fk_post_likes_post_id
    FOREIGN KEY (post_id) REFERENCES posts (id) 
    ON UPDATE CASCADE 
    ON DELETE RESTRICT,
  CONSTRAINT fk_post_likes_user_id
    FOREIGN KEY (user_id) REFERENCES users (id) 
    ON UPDATE CASCADE 
    ON DELETE RESTRICT
);
