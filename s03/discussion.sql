Create*/

INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("PSY");

/* INSERT INTO tableName (columnA, columnB, columnC) VALUES (columnAValue, columnBValue, coloumnCValue);*/

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2021-1-1", 2);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1991-1-1", 1);



INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 234, "K-pop", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 234, "OPM", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 230, "OPM", 2);

/* Retrieve*/
/* Retrieve all column values of songs table*/
SELECT * FROM songs;

/*Display and song name and genre of all the songs*/
SELECT song_name, genre FROM songs;

/*Display and song name and album_id of all the songs*/
SELECT song_name,album_id FROM songs;

/*Display the song_name of all the OPM song*/
SELECT song_name FROM songs WHERE genre = "OPM";
SELECT song_name, genre FROM songs WHERE genre = "OPM";

/*Update*/
/*UPDATE tableName set column = value WHERE column = value*/
UPDATE songs SET length = 240 WHERE song_name = "Kundiman";


/*Delete*/
DELETE FROM songs WHERE genre = "OPM" AND length > 200;

/*in where class we could add conditions using AND or OR*/

/*AND*/ -- /*All must be true for the result to be true*/
/* T and T and T and F will result to false*/
/* OR */ -- /*One true will result to true*/
/* T and T and T or F will result to True