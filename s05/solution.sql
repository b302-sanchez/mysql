/*Create an activity.sql inside s05 project and create the SQL code for the following:
    //-Return the customerName of the customers who are from the Philippines
    //-Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
    //-Return the product name and MSRP of the product named "The Titanic"
    //-Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
    //-Return the names of customers who have no registered state
    //-Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
    //-Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
    //-Return the customer numbers of orders whose comments contain the string 'DHL'
    //-Return the product lines whose text description mentions the phrase 'state of the art'
    //-Return the countries of customers without duplication
    //-Return the statuses of orders without duplication
    //-Return the customer names and countries of customers whose country is USA, France, or Canada
    //-Return the first name, last name, and office's city of employees whose offices are in Tokyo
    //-Return the customer names of customers who were served by the employee named "Leslie Thompson"
    //-Return the product names and customer name of products ordered by "Baane Mini Imports"
    -Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are located in the same country
    //-Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.
    //-Return the customer's name with a phone number containing "+81".*/


    SELECT customerName FROM customers WHERE country = 'Philippines';

    SELECT contactLastName, contactFirstName FROM customers WHERE customerName = 'La Rochelle Gifts';

    SELECT productName, MSRP FROM products WHERE productName = 'The Titanic';

    SELECT firstName, lastName FROM employees WHERE email = 'jfirrelli@classicmodelcars.com';

    SELECT customerName FROM customers WHERE state IS NULL;

    SELECT firstName, lastName, email FROM employees WHERE lastName = 'Patterson' AND firstName = 'Steve';

    SELECT customerName, country, creditLimit FROM customers WHERE country <> 'USA' AND creditLimit > 3000;

    SELECT DISTINCT o.customerNumber FROM orders o JOIN orderDetails od ON o.orderNumber = od.orderNumber WHERE comments LIKE '%DHL%';

    SELECT productLine FROM productLines WHERE textDescription LIKE '%state of the art%';

    SELECT DISTINCT country FROM customers;

    SELECT DISTINCT status FROM orders;

    SELECT customerName, country FROM customers WHERE country IN ('USA', 'France', 'Canada');

    SELECT e.firstName, e.lastName, o.city FROM employees e 
    JOIN offices o ON e.officeCode = o.officeCode WHERE o.city = 'Tokyo';

    SELECT c.customerName FROM customers c 
    JOIN employees e ON c.salesRepEmployeeNumber = e.employeeNumber WHERE e.firstName = 'Leslie' AND e.lastName = 'Thompson';

    SELECT p.productName, c.customerName FROM products p 
        JOIN orderDetails od ON p.productCode = od.productCode 
        JOIN orders o ON od.orderNumber = o.orderNumber 
        JOIN customers c ON o.customerNumber = c.customerNumber WHERE c.customerName = 'Baane Mini Imports';

    SELECT emp.firstName, emp.lastName, cust.customerName, off.country FROM employees AS emp 
        JOIN offices AS off ON emp.officeCode = off.officeCode 
        JOIN customers AS cust ON emp.employeeNumber = cust.salesRepEmployeeNumber WHERE cust.country = off.country;

    SELECT productName, quantityInStock FROM products WHERE productLine = 'Planes' AND quantityInStock < 1000;
    
    SELECT customerName FROM customers WHERE phone LIKE '%+81%';











